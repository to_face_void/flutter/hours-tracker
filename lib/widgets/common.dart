import 'package:flutter/material.dart';

Widget vSpacer(double space) {
  return SizedBox(height: space,);
}

Widget hSpacer(double space) {
  return SizedBox(width: space,);
}