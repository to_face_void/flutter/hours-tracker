import 'package:flutter/material.dart';

enum AppTheme {
  lightMode,
  darkMode
}

final Map<AppTheme, ThemeData> appThemeData = {
  AppTheme.lightMode: ThemeData(
    brightness: Brightness.light,
  ),
  AppTheme.darkMode: ThemeData(
    brightness: Brightness.dark,
  ),
};