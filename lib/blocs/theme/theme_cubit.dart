import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:hours_tracker/resources/app_theme.dart';

class ThemeCubit extends Cubit<ThemeData?> {
  ThemeCubit(): super(appThemeData[AppTheme.lightMode]);

  void changeTheme(AppTheme theme) => emit(appThemeData[theme]);
}