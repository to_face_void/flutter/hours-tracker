import 'package:flutter/material.dart';
import 'package:hours_tracker/pages/sign_in/widgets/email_input_field.dart';
import 'package:hours_tracker/pages/sign_in/widgets/password_input_field.dart';
import 'package:hours_tracker/widgets/common.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController? emailController;
  TextEditingController? passwordController;

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    emailController!.dispose();
    passwordController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 300,
          height: 400,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                EmailInputField(controller: emailController!),
                vSpacer(20),
                PasswordInputField(controller: passwordController!),
                // LoginButton(),
                // GoogleLoginButton(),
                // SignUpButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
