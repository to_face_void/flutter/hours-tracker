import 'package:flutter/material.dart';
import 'package:hours_tracker/blocs/theme/theme_cubit.dart';
import 'package:hours_tracker/resources/app_theme.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isDarkMode = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Switch(
              value: isDarkMode,
              onChanged: (value) {
                isDarkMode = value;
                setState(() {
                  if (isDarkMode) {
                    context.read<ThemeCubit>().changeTheme(AppTheme.darkMode);
                  } else {
                    context.read<ThemeCubit>().changeTheme(AppTheme.lightMode);
                  }
                });
              })
          ],
        ),
      ),
    );
  }
}
