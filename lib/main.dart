import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hours_tracker/blocs/theme/theme_cubit.dart';
import 'package:hours_tracker/pages/home/home_page.dart';
import 'package:hours_tracker/pages/sign_in/sign_in_page.dart';

void main() {
  runApp(const InitLayer());
}

class InitLayer extends StatelessWidget {
  const InitLayer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => ThemeCubit(),
        )
      ],
      child: const MyApp()
    );
  }
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeCubit, ThemeData?>(
      builder: (context, themeData)  => MaterialApp(
        title: 'Hours Tracker App',
        theme: themeData,
        home: const SignInPage(),
      )
    );
  }
}
